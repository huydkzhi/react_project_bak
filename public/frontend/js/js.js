
function getName(name){
	alert(name);
}
/*
Tạo component
 */

//CartComponent
var CartCpn = React.createClass({
	getName_bak:function(){
		getName(this.props.children);
	},
	layThongtin:function(){
		alert(this.props.children);
	},
	render: function () {
		return(
			<div className="container">
				<h3 className="title">Items in your cart</h3>
				<CartListCpn/>
				<ButtonCpn onClick={this.layThongtin}>Update cart</ButtonCpn>
				//cách 1
				<button onClick={this.getName_bak}>thongtin</button>
				//cách 2 
				<button onClick={()=>{getName(this.props.children)}}>laythongtinnhanh</button>
				<ButtonCpn>Continue shopping</ButtonCpn>
				<div className="checkoutbt">
					<ButtonCpn>Checkout</ButtonCpn>
				</div>
			</div>
		);	
	}
});
//CartListCpn
var CartListCpn = React.createClass({
	render: function () {
		return(
			<div className="table">
				<table className="table-div">
					<thead>
					<tr>
						<th>Product</th>
						<th></th>
						<th>Qty</th>
						<th>Total</th>
						<th></th>
					</tr>
					</thead>
					<tbody>
						<CartItemCpn img="frontend/img/smiley.gif" nameproduct="ajnomoto" costproduct="200.00vnd" />
						<CartItemCpn img="frontend/img/smiley.gif" nameproduct="ajnomoto" costproduct="200.00vnd" />
						<CartItemCpn img="frontend/img/smiley.gif" nameproduct="ajnomoto" costproduct="200.00vnd" />
					</tbody>
				</table>
			</div>
		);	
	}
});
//CartItemCpn
var CartItemCpn = React.createClass({
	render: function () {
		return(
			<tr>
				<td><img src={this.props.img} alt="Smiley face" height="42" width="42"/></td>
				<td>
				<p>{this.props.nameproduct}</p>
				<p>{this.props.costproduct}</p>
				</td>
				<td>
					<select>
						<option>1</option>
						<option>2</option>
					</select>
				</td>
				<td>ThanhGia</td>
				<td><ButtonCpn>Remove</ButtonCpn></td>
			</tr>
		);	
	}
});
//ButtonComponent
var ButtonCpn = React.createClass({
	render: function () {
		return(
			<div onClick={this.props.onClick}>
				<button type="button">{this.props.children}</button>
			</div>
		);	
	}
});

ReactDOM.render( 
	<div>
		<CartCpn cp="ReactJS" demo="hihi">hahaha</CartCpn>	
	</div>
, document.getElementById("root"));





